const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


const fullName =() => {
    const firstName = txtFirstName.value;
    const lastName = txtLastName.value;
    spanFullName.innerHTML = `${firstName} ${lastName}`;
    // console.log(firstName);
    // console.log(lastName);
    // console.log(spanFullName);
}

txtFirstName.addEventListener("keydown", fullName);
txtLastName.addEventListener("keydown", fullName);